﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ViewElement : MonoBehaviour
{
    [Header("Adds the Move In and Move Out Tween at this point in the View sequence.")]
    public int timelinePosition = 0;

    protected Tweener idleTween;
    protected Tweener moveInTween;
    protected Tweener moveOutTween;

    public virtual Tweener MoveIn()
    {
        return moveInTween;
    }

    public virtual Tweener MoveOut()
    {
        return moveOutTween;
    }

    public virtual Tweener Idle()
    {
        return idleTween;
    }

    protected void ResetTweens()
    {
        if (idleTween != null && idleTween.IsActive())
            idleTween.Kill(true);

        if (moveInTween != null && moveInTween.IsActive())
            moveInTween.Kill(true);

        if (moveOutTween != null && moveOutTween.IsActive())
            moveOutTween.Kill(true);
    }
}
