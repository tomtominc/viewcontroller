﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class View : MonoBehaviour
{
    public enum State
    {
        CLOSED,
        OPEN,
        MOVING_IN,
        MOVING_OUT
    }

    [Tooltip("Use the path to get to the correct view (optional-data can be the page if it's a scroll menu)")]
    public string path = "/SCENE_NAME/VIEW_NAME/optional-data";
    protected ViewElement[] elements;

    public State currentState;

    protected Sequence moveInSequence;
    protected Sequence moveOutSequence;
    protected Sequence idleSequence;

    public View Initialize()
    {
        if (gameObject.activeSelf)
            gameObject.SetActive(true);

        currentState = State.CLOSED;
        
        elements = GetComponentsInChildren < ViewElement >();
        return this;
    }

    public virtual void MoveIn(Hashtable p_data, Action<View> onComplete)
    {
        for (int i = 0; i < elements.Length; i++)
        {
            moveInSequence.Insert(elements[i].timelinePosition, elements[i].MoveIn());
        }

        moveInSequence.OnComplete(() => OnCompletedAction(State.OPEN, onComplete));
    }

    public virtual void MoveOut(Action<View> onComplete)
    {
        for (int i = 0; i < elements.Length; i++)
        {
            moveOutSequence.Insert(elements[i].timelinePosition, elements[i].MoveOut());
        }

        moveOutSequence.OnComplete(() => OnCompletedAction(State.CLOSED, onComplete));
    }

    public virtual void OnCompletedAction(State completeState, Action < View > onComplete)
    {
        ChangeState(completeState);

        if (onComplete != null)
            onComplete(this);
    }

    protected void ChangeState(State state)
    {
        currentState = state;
    }

    public virtual void Idle()
    {
        
    }
}
