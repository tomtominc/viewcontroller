﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ViewController : Singleton<ViewController>
{
    protected Dictionary<string,View> views;

    private void Awake()
    {
        if (views == null)
            Init();
    }

    protected void Init()
    {
        View[] l_views = GetComponentsInChildren < View >();

        views = new Dictionary < string, View >();

        for (int i = 0; i < l_views.Length; i++)
        {
            views.Add(l_views[i].name, l_views[i].Initialize());
        }

    }

    public static View OpenPath(string p_path, Action<View> onComplete)
    {
        string[] l_path = p_path.Split('/'); 

        string l_sceneName = l_path[0];
        string l_viewName = l_path[1];

        if (l_path.Length > 2)
        {
            Hashtable l_data = new Hashtable();

            for (int i = 2; i < l_path.Length; i++)
            {
                string[] l_dataSet = l_path[i].Split('=');
                l_data.Add(l_dataSet[0], l_dataSet[1]);
            }
        }

        return null;

    }

    public static View Open(object menu, Hashtable p_data, Action<View> p_onComplete)
    {
        if (Instance.views == null)
            Instance.Init();

        if (!Instance.views.ContainsKey(menu.ToString()))
        {
            Debug.LogWarningFormat("No Menu with key {0}", menu);
            return null;
        }

        View view = Instance.views[menu.ToString()];

        if (view.currentState == View.State.CLOSED)
            view.MoveIn(p_data, p_onComplete);

        return view;
            
    }

    public static List < View > Open(List<object> menu, Action<View> onComplete)
    {
        if (Instance.views == null)
            Instance.Init();

        return null;
    }

    public static View Close(object menu, Action<View> onComplete)
    {
        if (Instance.views == null)
            Instance.Init();

        if (!Instance.views.ContainsKey(menu.ToString()))
        {
            Debug.LogWarningFormat("No Menu with key {0}", menu);
            return null;
        }

        View view = Instance.views[menu.ToString()];

        if (view.currentState == View.State.OPEN)
            view.MoveOut(onComplete);

        return view;
    }

    public static List < View > Close(List <object> menu, Action<View> onComplete)
    {
        if (Instance.views == null)
            Instance.Init();

        return null;
    }

    public static void CloseAll(Action onComplete)
    {
        if (Instance.views == null)
            Instance.Init();
    }
}
